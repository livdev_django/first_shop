from django.db import models


# Create your models here.
class SliderProduct(models.Model):
    name = models.CharField(max_length=120)
    slider_pc = models.ImageField("image desktop", upload_to="uploads/slider_pc", default="", blank=True)
    slider_mo = models.ImageField('image mobile', upload_to="uploads/slider_mo", default='', blank=True)
    slider_product= models.ImageField('image product', upload_to="uploads/slider_product", default='', blank=True)
    text_html = models.TextField()
    url_slider = models.CharField()

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name="Слайдер"
