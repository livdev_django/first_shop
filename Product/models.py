from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=120)
    image_product = models.ImageField("image", upload_to="uploads/product", default="", blank=True)
    price = models.FloatField('цена')
    price_old = models.FloatField('старая цена')
    gramm = models.IntegerField('грамаж')
    text_smmall = models.CharField(max_length=120, default="")
    is_top = models.BooleanField()
    is_sale = models.BooleanField()

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name="Товар"
