from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from Product.models import Product as ProductData

# Create your views here.

def productall(request):
    data_product_top = ProductData.objects.all()[:6]
    context = {
        "data_product_top": data_product_top,
    }
    return render(request, "productshow.html",context)

def productshow(request,code):
    dataproduct = ProductData.objects.filter(id=code).values()
    context = {
        "dataproduct": dataproduct,
    }
    return render(request, "productshowitem.html", context)
@require_POST
def ajaxaddcart(request):
    if request.method == "POST":
        # Получаеи из сессию корзину товара
        carts = request.session.get('cart')
        # Товар в добавлеие в корзину
        addIdProduct = request.POST['idProduct']
        # Проверяем если сессия | если нету то создаем
        if carts:
            tempArra=[]
            countTovar=0
            # пробегаемся по корзине
            for x in carts:
                if(x[0]==addIdProduct):
                    x[1]+=1
                    countTovar+=1
                tempArra.append(x)
            # Если такой товар не нашел в массиве сессию до добовляем 1
            if countTovar==0 :
                sessionCart = [addIdProduct, 1]
                tempArra.append(sessionCart)
            # записывваем в сессию
            request.session['cart'] = tempArra
            print(tempArra)
            return HttpResponse('edit cart')
        else:
            # Если не было создана корзины то
            sessionCart = [[addIdProduct, 1]]
            request.session['cart'] = sessionCart
            return HttpResponse('new cart')
    return HttpResponse("errors add cart")

def showcart(request):
    carts = request.session.get('cart')
    inArrayID=[]
    for x in carts:
        inArrayID.append(int(x[0]))
    datasProducts=ProductData.objects.filter(pk__in=inArrayID)
    inArrayProducts = []
    sumProducts=0
    itogPrice=0;
    for datasProduct in datasProducts:
        for x in carts:
            if datasProduct.id==int(x[0]):
                inArrayProducts.append([datasProduct.id,datasProduct.image_product,datasProduct.price,datasProduct.name,x[1],datasProduct.text_smmall,datasProduct.price*x[1]])
                sumProducts+=datasProduct.price*x[1]
    dostavkPrice=300
    minItogPrice=2000
    if(sumProducts>minItogPrice):
        itogPrice=sumProducts
    else:
        itogPrice=sumProducts+dostavkPrice
    context = {
        "cart": carts,
        "datasProducts" : datasProducts,
        "inArrayProducts": inArrayProducts,
        "sumProducts":sumProducts,
        "dostavkPrice":dostavkPrice,
        "itogPrice":itogPrice,
        "minItogPrice":minItogPrice
    }
    return render(request, "cart.html",context)
