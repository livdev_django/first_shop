from datetime import datetime
from django.http import HttpResponse
from django.shortcuts import render
from SliderProduct.models import SliderProduct
from Product.models import Product as ProductData

# Create your views here.

def homepage(request):
    data_sliders=SliderProduct.objects.all()
    data_product_top=ProductData.objects.all()[:6]
    context = {
        "data_sliders": data_sliders,
        "data_product_top": data_product_top,
    }
    return render(request,"index.html",context)